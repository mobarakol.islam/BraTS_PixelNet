function [sampled, label] = seg_label_provider_4C(cnn_input_size,  segpath, sample_size, flip)

% first get the mask
try,

	%cnn_input_size = options.cnn_input_size;
	bmask = imread([segpath,'_OT.png']);

% 	if(flip)
% 		bmask = flipdim(bmask,2);
% 	end

	bmask = reshape(bmask, 1, cnn_input_size*cnn_input_size);
% 	tmask = find(bmask < 255);
% 
% 	if((length(tmask)>0) && (length(tmask) < sample_size))
% 		tmask = repmat(tmask, 1, ceil(sample_size/length(tmask)));
% 	end

	% then sample from each
	sampled = zeros(3, sample_size, 'single');
	label = zeros(1, sample_size, 'single');
    if length((unique(bmask)))>1
        [a,b]=hist(bmask,unique(single(bmask)));
        [val ind] = sort(bmask,'descend');
        randa2 = randperm(a(2));
        tmask = zeros(1,sample_size);

        if a(2) < sample_size/2
            tmask(1:a(2)) = ind(randa2);
            tmask(a(2)+1:end) = randsample(ind(a(2)+1:end),sample_size-a(2));
        else     
            tmask(1:sample_size/2) = ind(randa2(1:sample_size/2));
            tmask(sample_size/2+1:end) = randsample(ind(sample_size/2+1:end),sample_size/2);
        end

        %tmask = randsample(tmask,sample_size);
        rand_ids = randperm(length(tmask));
        tmask = tmask(rand_ids);
    else
	    tmask = find(bmask < 255);
		if((length(tmask)>0) && (length(tmask) < sample_size))
			tmask = repmat(tmask, 1, ceil(sample_size/length(tmask)));
		end
        tmask = randsample(tmask,sample_size);
    end
        
	[y,x] = ind2sub([cnn_input_size, cnn_input_size],tmask);
	label(:) = bmask(tmask);

	%
	sampled(2,:) = y - 1; % matlab index start from 1 where image index is zero
	sampled(3,:) = x - 1;
catch,
	keyboard;
end


end

