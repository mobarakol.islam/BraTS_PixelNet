% this is an example code for training a PixelNet model using caffe
% here we consider a 224x224 image 
% uniform sampling of pixels in an image, used for segmentation
function BraTS_train_HeMIS()

    addpath(['../caffe/matlab']);
	trained_models = [ 'trained_models_all/'];
	if(~isdir(trained_models))
		mkdir(trained_models);
	end

	% --
	caffe.reset_all;
	caffe.set_device(0);
	caffe.set_mode_gpu; 
    solverpath = ['../models/BRATS_solver_HeMIS.prototxt'];
	solverpath_init = ['../models/BRATS_solver.prototxt'];
	initmodelpath = ['../models/VGG16_fconv.caffemodel'];

    datasetfile = ['../data/dataset_BRATS_4C.mat'];
    load(datasetfile, 'imagelist');
    imagelist = repmat(imagelist, 10,1);
	rand_ids = randperm(length(imagelist));
	imagelist = imagelist(rand_ids);
    li = length(imagelist);
	caffe.reset_all;

	% initialize network
	solver = caffe.get_solver(solverpath);
    solver_init = caffe.get_solver(solverpath_init);
	% load the model
	solver_init.net.copy_from(initmodelpath);
	solver = get_weights_2(solver,solver_init );
	% --
    maxsize = 240;
    cnn_input_size = 240;
    segsamplesize = 2000;
    segimbatch = 5;
    segbatchsize = (segimbatch)*(segsamplesize);
    segepoch = 80;
    saveEpoch = 1;
    meanvalue = [26.70, 37.47, 23.20, 23.74]; % Flair, T1, T1c, T2
    trainFlip = 1;
    seed = 1989;
	input_data_1 = zeros(maxsize, maxsize, 2, segimbatch, 'single');
	input_data_2 = zeros(maxsize, maxsize, 2, segimbatch, 'single');
    %input_data = zeros(maxsize, maxsize, 3, segimbatch, 'single');
    input_index = zeros(3,segbatchsize, 'single');
    input_label = zeros(1,segbatchsize, 'single');
	solver.net.blobs('data_1').reshape([maxsize, maxsize,2, segimbatch]);
	solver.net.blobs('data_2').reshape([maxsize, maxsize,2, segimbatch]);
    solver.net.blobs('pixels').reshape([3,segbatchsize]);
    solver.net.blobs('labels').reshape([1,segbatchsize]);

	% set up memory
	solver.net.forward_prefilled();
	% then start training
	oldrng = rng;
	rng(seed, 'twister');
    
    for epoch = 1:segepoch
        index = randperm(li);
        for i = 1:segimbatch:li
            j = i+segimbatch-1;
            if j > li
                continue;
            end

            st = 1;
            ed = segsamplesize;
            im = 0;
            for k=i:j
                ik = index(k);
                if trainFlip 
                    flip = rand(1) > 0.5;
                end
                im_data = seg_image_provider_4C(imagelist{ik}, flip);
            	im_size = length(im_data);
                im_data(:,:,1) = im_data(:,:,1) - meanvalue(4);  
                im_data(:,:,2) = im_data(:,:,2) - meanvalue(1);
                im_data(:,:,3) = im_data(:,:,3) - meanvalue(3);
            	im_data(:,:,4) = im_data(:,:,4) - meanvalue(2);
	            [sampled, label] = seg_label_provider_4C(...
	            im_size, imagelist{ik}, segsamplesize, flip);

                % notice the zero-index vs. the one-index
                sampled(1,:) = im;
                sampled(2,:) = sampled(2,:) ; % index start from 101 after adding padding 100 around the image
                sampled(3,:) = sampled(3,:) ;

                im = im + 1;
                input_data_1(1:cnn_input_size,1:cnn_input_size, :, im) = im_data(:,:,1:2);
				input_data_2(1:cnn_input_size,1:cnn_input_size, :, im) = im_data(:,:,3:4);
                input_index(:, st:ed) = sampled;
                input_label(st:ed) = label;
                st = st + segsamplesize;
                ed = ed + segsamplesize;
            end

	        solver.net.blobs('data_1').set_data(input_data_1);
	        solver.net.blobs('data_2').set_data(input_data_2);
            solver.net.blobs('pixels').set_data(input_index);
            solver.net.blobs('labels').set_data(input_label);
            solver.step(1);
            % clean up everything
	        input_data_1(:) = 0;
	        input_data_2(:) = 0;
        end
        % add another condition
        if mod(epoch, saveEpoch) == 0 && epoch < segepoch
            epochfile = [trained_models, sprintf('(%02d).caffemodel',epoch)];
            solver.net.save(epochfile);
        end

    end

	targetfile = [trained_models,'final_model.caffemodel'];
	solver.net.save(targetfile);
	caffe.reset_all;
	rng(oldrng);
	caffe.reset_all;
end
