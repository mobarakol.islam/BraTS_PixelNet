function [im_data] = seg_image_provider_3C(impath, flip)

% --
im = imread([impath,'_T1.png']);
%im_size = length(im);
im(:,:,2) = imread([impath,'_T1c.png']);
im(:,:,3) = imread([impath,'_Flair.png']);
im(:,:,4) = imread([impath,'_T2.png']);

% if(flip)
% 	im = flipdim(im,2);
% end

%
% color_code = (1.3-0.8)*rand(3,1) + 0.8;
% for c = 1:3
% 	 im(:,:,c) = color_code(c)*im(:,:,c); 
% end


% setting the im-data
im_data = single(im(:, :, [4, 3, 2, 1]));  
im_data = permute(im_data, [2, 1, 3]);  
im_data = single(im_data);
% subtract mean_data (already in W x H x C, BGR)
end
