function msolver = get_weights_2(msolver, msolver_init)
%caffe.reset_all;
%caffe.set_device(0);
%caffe.set_mode_gpu;
phase = 'test';

% Initialize a network
%net = caffe.Net(deploy_file, weights_file, phase);
nlayers = length(msolver_init.net.layer_names);

% get params of each layer who has parameters
trans_params = struct('weights', 'layer_names');
trans_params_cnt = 0;
for i = 1:nlayers
  weights = cell(2, 1);
  try
    weights{1} = msolver_init.net.params(msolver_init.net.layer_names{i}, 1).get_data();
  catch
    % do nothing
  end
  try
    weights{2} = msolver_init.net.params(msolver_init.net.layer_names{i}, 2).get_data();
  catch
    % do nothing
  end
  if ~isempty(weights{1}) || ~isempty(weights{2})
    fprintf('%s has params\n', msolver_init.net.layer_names{i});
    %myweights = rand (msolver.net.params(msolver.net.layer_names{i+1}, 1).shape);
    if i == 2
        %myweights(:,:,1:3,:) = weights{1};
        msolver.net.params(msolver.net.layer_names{i+1}, 1).set_data(weights{1}(:,:,1:2,:));
        myweights = rand (msolver.net.params(msolver.net.layer_names{i+1}, 1).shape);
        myweights(:,:,1,:) = weights{1}(:,:,3,:);
        myweights(:,:,2,:) = (weights{1}(:,:,2,:) + weights{1}(:,:,3,:))/2;
        msolver.net.params(msolver.net.layer_names{i+1+43}, 1).set_data(myweights);
        %msolver.net.params(msolver.net.layer_names{i+1+43+43}, 1).set_data(weights{1}(:,:,3,:));
        msolver.net.params(msolver.net.layer_names{i+1}, 2).set_data(weights{2});
        msolver.net.params(msolver.net.layer_names{i+1+43}, 2).set_data(weights{2});
        %msolver.net.params(msolver.net.layer_names{i+1+43+43}, 2).set_data(weights{2});
        
    elseif i == 45
        msolver.net.params(msolver.net.layer_names{i+45}, 1).set_data(repmat(weights{1},[2 1]));
        msolver.net.params(msolver.net.layer_names{i+45}, 2).set_data(weights{2});
    elseif i == 48 || i == 51
        msolver.net.params(msolver.net.layer_names{i+45}, 1).set_data(weights{1});
        msolver.net.params(msolver.net.layer_names{i+45}, 2).set_data(weights{2});
    else
        msolver.net.params(msolver.net.layer_names{i+1}, 1).set_data(weights{1});
        msolver.net.params(msolver.net.layer_names{i+1+43}, 1).set_data(weights{1});
        %msolver.net.params(msolver.net.layer_names{i+1+43+43}, 1).set_data(weights{1});
        msolver.net.params(msolver.net.layer_names{i+1}, 2).set_data(weights{2});
        msolver.net.params(msolver.net.layer_names{i+1+43}, 2).set_data(weights{2});
        %msolver.net.params(msolver.net.layer_names{i+1+43+43}, 2).set_data(weights{2});
        %myweights = weights{1};
    end
    %msolver.net.params(msolver.net.layer_names{i}, 1).set_data(myweights);

%     trans_params_cnt = trans_params_cnt + 1;
%     trans_params(trans_params_cnt).weights = weights;    
%     trans_params(trans_params_cnt).layer_names = msolver_init.net.layer_names{i};
   end
end

% clear network
%caffe.reset_all();

