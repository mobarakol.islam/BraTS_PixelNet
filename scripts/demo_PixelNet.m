% demo code for semantic segmentation--
% NOTE - this demo code is for a single scale only --
% a minor modification is required for multi-scale
clc; clear all;
addpath(['../caffe/matlab']);

%
epoch = '05';
seg_predict = ['./seg_prediction_all/',num2str(epoch),'_valid/'];
if(~isdir(seg_predict))
        mkdir(seg_predict);
end

% initialize caffe
net_file     = ['trained_models_all/(',num2str(epoch),').caffemodel'];
deploy_file  = ['../models/', 'BRATS_deploy.prototxt']; 
%load([NET_FILE_PATH, 'colormap.mat']);

% set the gpu --
% if not using GPU, set it to CPU mode.
gpu_id = 1;
caffe.reset_all;
caffe.set_device(gpu_id);
caffe.set_mode_gpu;
net = caffe.Net(deploy_file, net_file, 'test');

cnn_input_size = 240;
crop_height = 240; crop_width = 240;
image_mean = cat(3,  26.704242*ones(cnn_input_size),...
		     23.208244*ones(cnn_input_size),...
		     23.7459983*ones(cnn_input_size));

addpath('/home/mobarak/Datasets/Nii/');
%MRI_dir = '/home/mobarak/SEG/BRATS17/HGG_LGG/';
MRI_dir = '/home/mobarak/SEG/BRATS17/Brats17ValidationData/';
%MRI_dir = '/home/mobarak/SEG/BRATS17/Brats17TestingData/';
datasets = dir(MRI_dir);
datasets = datasets([datasets.isdir] & ~strncmpi('.', {datasets.name}, 1));
num_classes = length(datasets);
for p = 1:num_classes
    disp('p no:')
    disp(p);
    channels_dir = [MRI_dir,char(datasets(p).name),'/'];
    channels = dir(channels_dir);
    channels = channels(~strncmpi('.', {channels.name}, 1));
    my_NII = zeros([240 240 155]);
    for j=1:length(channels)
        moda = [channels_dir,char(channels(j).name)];
        nii = load_nii(moda);
        if moda(end-7) == 'r'
            %flair = uint8(255 * mat2gray(nii.img));
            flair = mat2gray(nii.img);
         %elseif moda(end-7) == 'g'
            %seg = uint8(nii.img);
            %seg(seg == 4) = 3;
        elseif moda(end-7) == '1'
            t1 = 0;%uint8(255 * mat2gray(nii.img));
        elseif moda(end-7) == 'e'
            %t1ce = uint8(255 * mat2gray(nii.img));
            t1ce = mat2gray(nii.img);
        elseif moda(end-7) == '2'
            %t2 =  uint8(255 * mat2gray(nii.img));
            t2 = mat2gray(nii.img);
        end               
    end 
    for slice = 1:155
        %disp(slice)
        img_data = zeros([240 240 3]);
        if length(unique(flair(:,:,slice)))>1 || length(unique(t1ce(:,:,slice)))>1 || length(unique(t2(:,:,slice)))>1

            img_data(:,:,1) = fliplr(imrotate(flair(:,:,slice),90));
            img_data(:,:,2) = fliplr(imrotate(t1ce(:,:,slice),90));
            img_data(:,:,3) = fliplr(imrotate(t2(:,:,slice),90));

            % for each image in the img_set
            %for i = 1:length(img_data)          
            ith_Img = im2uint8(img_data);
            %
%             save_file_name = [conv_cache, img_data{i}];
%             if(exist([save_file_name], 'file'))
%                     continue;
%             end

            j_ims = single(ith_Img(:,:,[3 2 1]));
            j_tmp = imresize(j_ims, [cnn_input_size, cnn_input_size], ...
                               'bilinear', 'antialiasing', false);
            j_tmp = j_tmp - image_mean;
            ims(:,:,:,1) = permute(j_tmp, [2 1 3]);	


            %
            net.blobs('data').reshape([crop_height, crop_width, 3, 1]);
            net.blobs('pixels').reshape([3,crop_height*crop_width]);
            h = crop_height;
            w = crop_width;
            hw = h * w;

            xs = reshape(repmat(0:w-1,h,1), 1, hw);
            ys = reshape(repmat(0:h-1,w,1)', 1, hw);


            % set the image data --
            input_data = zeros(crop_height,crop_width,3,1);
            input_data(1:crop_width, 1:crop_width, :, 1) = ims;
            net.blobs('data').set_data(input_data);

            % set the pixels --
            input_index = zeros(3, 240*240);
            input_index(1,:) = 0;
            input_index(2,:) = xs;
            input_index(3,:) = ys;
            net.blobs('pixels').set_data(input_index);

            % feed forward the values --
            net.forward_prefilled();
            out = net.blobs('f_score').get_data();

            % reshape the data --
            f2 = out';
            [~, f2] = max(f2, [], 2);
            f2 = f2 -1;
            f2 = reshape(f2, [240, 240,1]);
            f2 = permute(f2, [2,1,3]);
            f2(f2 == 3) = 4;
            my_NII(:,:,slice) = fliplr(imrotate(f2,-90));

        end
    end
    
    NII = make_nii(my_NII);
    save_nii(NII, [seg_predict,datasets(p).name,'.nii.gz']);    
end

% reset caffe
caffe.reset_all;
