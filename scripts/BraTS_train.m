% this is an example code for training a PixelNet model using caffe
% here we consider a 224x224 image 
% uniform sampling of pixels in an image, used for segmentation
function BraTS_train()

    addpath(['../caffe/matlab']);
	trained_models = [ 'trained_models_all/'];
	if(~isdir(trained_models))
		mkdir(trained_models);
	end

	% --
	caffe.reset_all;
	caffe.set_device(0);
	caffe.set_mode_gpu;
    
    solverpath = ['../models/BRATS_solver.prototxt'];
	initmodelpath = ['../models/VGG16_fconv.caffemodel'];

    datasetfile = ['../data/dataset_BRATS_3C_all.mat'];
    load(datasetfile, 'imagelist', 'seglist');
    imagelist = repmat(imagelist, 10,1);
	seglist = repmat(seglist,10,1);
	rand_ids = randperm(length(imagelist));
	imagelist = imagelist(rand_ids);
	seglist = seglist(rand_ids);
    li = length(imagelist);
	caffe.reset_all;

	% initialize network
	solver = caffe.get_solver(solverpath);
	% load the model
	solver.net.copy_from(initmodelpath);

	% --
    maxsize = 240;
    cnn_input_size = 240;
    segsamplesize = 2000;
    segimbatch = 5;
    segbatchsize = (segimbatch)*(segsamplesize);
    segepoch = 80;
    saveEpoch = 1;
    %meanvalue = [47.3589, 23.2563, 25.1298];
    meanvalue = [26.70, 23.20, 23.74]; % Flair, T1c, T2
    trainFlip = 1;
    seed = 1989;
    
    input_data = zeros(maxsize, maxsize, 3, segimbatch, 'single');
    input_index = zeros(3,segbatchsize, 'single');
    input_label = zeros(1,segbatchsize, 'single');
    solver.net.blobs('data').reshape([maxsize, maxsize, 3, segimbatch]);
    solver.net.blobs('pixels').reshape([3,segbatchsize]);
    solver.net.blobs('labels').reshape([1,segbatchsize]);

	% set up memory
	solver.net.forward_prefilled();
	% then start training
	oldrng = rng;
	rng(seed, 'twister');
    
    for epoch = 1:segepoch
        index = randperm(li);
        for i = 1:segimbatch:li
            j = i+segimbatch-1;
            if j > li
                continue;
            end

            st = 1;
            ed = segsamplesize;
            im = 0;
            for k=i:j
                ik = index(k);
                if trainFlip 
                    flip = rand(1) > 0.5;
                end
                im_data = seg_image_provider_3C(cnn_input_size, imagelist{ik}, flip);
                im_data(:,:,1) = im_data(:,:,1) - meanvalue(1);  
                im_data(:,:,2) = im_data(:,:,2) - meanvalue(2);
                im_data(:,:,3) = im_data(:,:,3) - meanvalue(3);
                [sampled, label] = seg_label_provider_3C(...
                cnn_input_size, seglist{ik}, segsamplesize, flip);

                % notice the zero-index vs. the one-index
                sampled(1,:) = im;
                sampled(2,:) = sampled(2,:) ; % index start from 101 after adding padding 100 around the image
                sampled(3,:) = sampled(3,:) ;

                im = im + 1;
                input_data(1:cnn_input_size,....
                1:cnn_input_size, :, im) = im_data;
                input_index(:, st:ed) = sampled;
                input_label(st:ed) = label;
                st = st + segsamplesize;
                ed = ed + segsamplesize;
            end

            solver.net.blobs('data').set_data(input_data);
            solver.net.blobs('pixels').set_data(input_index);
            solver.net.blobs('labels').set_data(input_label);
            solver.step(1);
            % clean up everything
            input_data(:) = 0;
        end
        % add another condition
        if mod(epoch, saveEpoch) == 0 && epoch < segepoch
            epochfile = [trained_models, sprintf('(%02d).caffemodel',epoch)];
            solver.net.save(epochfile);
        end

    end

	targetfile = [trained_models,'final_model.caffemodel'];
	solver.net.save(targetfile);
	caffe.reset_all;
	rng(oldrng);
	caffe.reset_all;
end