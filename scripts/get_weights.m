function msolver = get_weights(msolver, msolver_init)
%caffe.reset_all;
%caffe.set_device(0);
%caffe.set_mode_gpu;
phase = 'test';

% Initialize a network
%net = caffe.Net(deploy_file, weights_file, phase);
nlayers = length(msolver_init.net.layer_names);

% get params of each layer who has parameters
trans_params = struct('weights', 'layer_names');
trans_params_cnt = 0;
for i = 1:nlayers
  weights = cell(2, 1);
  try
    weights{1} = msolver_init.net.params(msolver_init.net.layer_names{i}, 1).get_data();
  catch
    % do nothing
  end
  try
    weights{2} = msolver_init.net.params(msolver_init.net.layer_names{i}, 2).get_data();
  catch
    % do nothing
  end
  if ~isempty(weights{1}) || ~isempty(weights{2})
    fprintf('%s has params\n', msolver_init.net.layer_names{i});
    myweights = rand (msolver.net.params(msolver.net.layer_names{i}, 1).shape);
    if i == 2
        myweights(:,:,1:3,:) = weights{1};
        myweights(:,:,4,:) = weights{1}(:,:,2,:);
    else
        myweights = weights{1};
    end
    msolver.net.params(msolver.net.layer_names{i}, 1).set_data(myweights);
    msolver.net.params(msolver.net.layer_names{i}, 2).set_data(weights{2});
%     trans_params_cnt = trans_params_cnt + 1;
%     trans_params(trans_params_cnt).weights = weights;    
%     trans_params(trans_params_cnt).layer_names = msolver_init.net.layer_names{i};
   end
end

% clear network
%caffe.reset_all();

